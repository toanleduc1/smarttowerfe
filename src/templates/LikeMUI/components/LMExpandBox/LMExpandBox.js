import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';


const LMExpandBox = styled(Box)({
  flexGrow: 1,
});

export default LMExpandBox;