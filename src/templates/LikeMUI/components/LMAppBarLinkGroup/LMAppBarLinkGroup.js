import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';

import LMAppBarLink from './LMAppBarLink';
import { APPBAR_LINKS } from '../../settings';


const StyledLMAppBarLinkGroup = styled(Box)(({ theme }) => ({
  paddingLeft: theme.spacing(5),
  [theme.breakpoints.down('sm')]: {
    display: 'none',
  }
}));

const LMAppBarLinkGroup = () => {
  return (
    <StyledLMAppBarLinkGroup>
      {
        APPBAR_LINKS.map((link, index) => (
          <LMAppBarLink
            key={index}
            path={link.path}
            name={link.name}
          />
        ))
      }
    </StyledLMAppBarLinkGroup>
  );
}

export default LMAppBarLinkGroup;