import { Button } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';


const StyledLMAppBarLink = styled(Button)({
  color: '#000',
  fontWeight: 'bold',
  textDecoration: 'none',
  textTransform: 'none',
});

const LMAppBarLink = (props) => {
  const { t } = useTranslation();
  
  return (
    <StyledLMAppBarLink
      component={Link}
      to={props.path}
    >
      {t(props.name)}
    </StyledLMAppBarLink>
  );
}

export default LMAppBarLink;