import { Link } from 'react-router-dom';

import logo from '../../images/logo.png';


const LMAppBarLogo = () => {
  return (
    <Link
      to='/home'
    >
      <img
        src={logo}
        alt='Like MUI logo'
        height={40}
      />
    </Link>
  );
}

export default LMAppBarLogo;