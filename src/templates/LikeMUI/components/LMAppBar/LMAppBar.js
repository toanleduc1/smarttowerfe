import { AppBar, Toolbar } from '@mui/material';
import { styled } from '@mui/material/styles';

import LMAppBarLogo from '../LMAppBarLogo';
import LMAppBarLinkGroup from '../LMAppBarLinkGroup';
import LMExpandBox from '../LMExpandBox';
import LMAppBarSettings from '../LMAppBarSettings';


const StyledLMAppBar = styled(AppBar)(({ theme }) => ({
  backdropFilter: 'blur(10px)',
  [theme.breakpoints.up('md')]: {
    paddingLeft: theme.spacing(20),
    paddingRight: theme.spacing(20),
  }
}));

const LMAppBar = () => {
  return (
    <StyledLMAppBar
      color='transparent'
      position='sticky'
    >
      <Toolbar
        variant='dense' // Make app bar thinner
      >
        <LMAppBarLogo />
        <LMAppBarLinkGroup />
        <LMExpandBox />
        <LMAppBarSettings />
      </Toolbar>
    </StyledLMAppBar>
  );
}

export default LMAppBar;