import React from 'react';
import { List, ListSubheader } from '@mui/material';
import { useTranslation } from 'react-i18next';

import LMAppBarSettingsLanguageButton from './LMAppBarSettingsLanguageButton';
import { LANGUAGES } from '../../settings';


const LMAppBarSettingsLanguage = () => {
  const { t } = useTranslation();

  return (
    <List
      dense={true} // Smaller list item
      subheader={
        <ListSubheader>
          {t('Language')}
        </ListSubheader>
      }
    >
      {
        LANGUAGES.map(language => (
          <LMAppBarSettingsLanguageButton
            key={language.id}
            id={language.id}
            name={language.name}
          />
        ))
      }
    </List>
  )
}

export default LMAppBarSettingsLanguage;