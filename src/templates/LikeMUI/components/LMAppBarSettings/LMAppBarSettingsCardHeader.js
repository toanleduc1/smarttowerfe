import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import { CardHeader, IconButton } from '@mui/material';
import { useContext } from 'react';
import { useTranslation } from 'react-i18next';

import { context } from './LMAppBarSettingsContext';


const LMAppBarSettingsCardHeader = () => {
  const { t } = useTranslation();
  const { dispatch } = useContext(context);

  return (
    <CardHeader
      title={t('Settings')}
      titleTypographyProps={{ sx: { fontWeight: 'bold', fontSize: '1.2rem' } }}
      action={
        <IconButton
          color='primary'
          onClick={() => dispatch({ type: 'close-drawer' })}
        >
          <CloseOutlinedIcon />
        </IconButton>
      }
    />
  );
}

export default LMAppBarSettingsCardHeader;