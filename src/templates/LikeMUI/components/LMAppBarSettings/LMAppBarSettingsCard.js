import { Card, CardContent, Divider } from '@mui/material';
import { styled } from '@mui/material/styles';

import LMAppBarSettingsCardHeader from './LMAppBarSettingsCardHeader';
import LMAppBarSettingsLanguage from './LMAppBarSettingsLanguage';


const StyledCard = styled(Card)(({ theme }) => ({
  width: theme.spacing(45),
  height: '100%',
  borderTopLeftRadius: theme.spacing(1),
}));

const LMAppBarSettingsCard = () => {
  return (
    <StyledCard>
      <LMAppBarSettingsCardHeader />
      <Divider />
      <CardContent>
        <LMAppBarSettingsLanguage />
      </CardContent>
    </StyledCard>
  );
}

export default LMAppBarSettingsCard;