import { createContext, useReducer } from 'react';


const init = {
  open: false,
}

const context = createContext(init);

const LMAppBarSettingsProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'open-drawer':
        return { open: true };
      case 'close-drawer':
        return { open: false };
      default:
        return state;
    }
  }, init);

  return (
    <context.Provider
      value={{ state, dispatch }}
    >
      {children}
    </context.Provider>
  );
}

export { context, LMAppBarSettingsProvider };