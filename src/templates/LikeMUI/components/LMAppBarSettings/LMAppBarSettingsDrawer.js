import { Drawer } from '@mui/material';
import { useContext } from 'react';

import LMAppBarSettingsCard from './LMAppBarSettingsCard';
import { context } from './LMAppBarSettingsContext';


const LMAppBarSettingsDrawer = () => {
  const { state, dispatch } = useContext(context);

  return (
    <Drawer
      anchor='right'
      open={state.open}
      ModalProps={{ onBackdropClick: () => dispatch({ type: 'close-drawer' }) }}
      PaperProps={{ sx: { background: 'transparent' } }}
    >
      <LMAppBarSettingsCard />
    </Drawer>
  );
}

export default LMAppBarSettingsDrawer;