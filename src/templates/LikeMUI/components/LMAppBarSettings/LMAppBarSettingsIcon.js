import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';
import { Button, Tooltip } from '@mui/material';
import { styled } from '@mui/material/styles';
import { useContext } from 'react';

import { context } from './LMAppBarSettingsContext';


const StyledLMAppBarSettingsIcon = styled(Button)(({ theme }) => ({
  color: '#fc3468', // pink
  borderColor: '#e0e3e7', // light gray
  borderRadius: theme.spacing(1),
  padding: theme.spacing(0.5),
  marginInline: theme.spacing(1), // Space between buttons
  minWidth: theme.spacing(0), // Smaller button width
  '&:hover': {
    borderColor: '#e0e3e7', // light gray
  }
}));


const LMAppBarSettingsIcon = () => {
  const { dispatch } = useContext(context);

  return (
    <Tooltip
      title='Settings'
    >
      <StyledLMAppBarSettingsIcon
        variant='outlined'
        onClick={() => dispatch({ type: 'open-drawer' })}
      >
        <SettingsOutlinedIcon />
      </StyledLMAppBarSettingsIcon>
    </Tooltip>
  );
}

export default LMAppBarSettingsIcon;