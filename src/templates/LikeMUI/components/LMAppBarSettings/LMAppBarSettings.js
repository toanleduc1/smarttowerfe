import LMAppBarSettingsIcon from './LMAppBarSettingsIcon';
import LMAppBarSettingsDrawer from './LMAppBarSettingsDrawer';

import { LMAppBarSettingsProvider } from './LMAppBarSettingsContext';


const LMAppBarSettings = () => {
  return (
    <LMAppBarSettingsProvider>
      <LMAppBarSettingsIcon />
      <LMAppBarSettingsDrawer />
    </LMAppBarSettingsProvider>
  );
}

export default LMAppBarSettings;