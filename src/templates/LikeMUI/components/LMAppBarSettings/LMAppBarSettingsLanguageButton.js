import { ListItemButton, ListItemText } from '@mui/material';
import { useTranslation } from 'react-i18next';


const LMAppBarSettingsLanguageButton = (props) => {
  const { i18n } = useTranslation();

  return (
    <ListItemButton
      divider={true}
      selected={props.id === i18n.language}
      onClick={() => i18n.changeLanguage(props.id)}
    >
      <ListItemText
        primary={props.name}
      />
    </ListItemButton>
  )
}

export default LMAppBarSettingsLanguageButton;