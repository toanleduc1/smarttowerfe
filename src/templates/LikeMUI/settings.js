export const APPBAR_LINKS = [
  {name: 'Places', path: '/places'},
  {name: 'Rental management', path: '/management'},
  {name: 'Technical studies', path: '/technical'}
];

export const LANGUAGES = [
  {id: 'en', name: 'English'},
  {id: 'vi', name: 'Tiếng Việt'},
];

export const PREAMBLE = 'Quickly rent a radio location';