import { createContext, useReducer } from 'react';


const init = {
  cities: [],
  districts: [],
}

const context = createContext(init);

const FilterProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'update-cities':
        return { cities: action.payload, districts: [] };
      case 'update-districts':
        return { ...state, districts: action.payload };
      default:
        return state;
    }
  }, init);

  return (
    <context.Provider
      value={{ state, dispatch }}
    >
      {children}
    </context.Provider>
  );
}

export { context, FilterProvider };