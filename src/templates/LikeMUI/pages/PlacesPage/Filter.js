import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';

import CitiesFilter from './CitiesFilter';
import DistrictsFilter from './DistrictsFilter';


const StyledFilter = styled(Box)(({ theme }) => ({
  paddingTop: theme.spacing(7),
  paddingBottom: theme.spacing(2),
  paddingLeft: theme.spacing(5),
  paddingRight: theme.spacing(5),
  [theme.breakpoints.up('sm')]: {
    display: 'flex',
    paddingLeft: theme.spacing(20),
    paddingRight: theme.spacing(20),
  },
}));

const Filter = () => {
  return (
    <StyledFilter>
        <CitiesFilter />
        <DistrictsFilter />
    </StyledFilter>
  );
}

export default Filter;