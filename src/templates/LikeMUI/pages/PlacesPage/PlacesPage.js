import React from 'react';
import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';

import LMAppBar from '../../components/LMAppBar';
import Filter from './Filter';
import FilterCardGroup from './FilterCardGroup';
import { FilterProvider } from './FilterContext';


const StyledPlacesPage = styled(Box)(({ theme }) => ({
  height: `calc(100vh - ${theme.mixins.toolbar.minHeight}px)`,
}));

const PlacesPage = () => {
  return (
    <React.Fragment>
      <LMAppBar />
      <StyledPlacesPage>
        <FilterProvider>
          <Filter />
          <FilterCardGroup />
        </FilterProvider>
      </StyledPlacesPage>
    </React.Fragment>
  );
}

export default PlacesPage;