import { Autocomplete, TextField } from '@mui/material';
import { styled } from '@mui/material/styles';
import { useContext } from 'react';

import towers from '../../data/towers.json';
import { context } from './FilterContext';


const StyledCitiesFilter = styled(Autocomplete)(({ theme }) => ({
  '& label.Mui-focused': {
    color: '#fc3468', // pink
  },
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderRadius: theme.spacing(2),
    },
    '&.Mui-focused fieldset': {
      borderColor: '#fc3468', // pink
    },
  },
  marginTop: theme.spacing(1),
  marginBottom: theme.spacing(1),
  [theme.breakpoints.up('sm')]: {
    maxWidth: theme.spacing(28),
    marginInline: theme.spacing(1),
  },
}));


const CitiesFilter = () => {
  const { state, dispatch } = useContext(context);
  const cities = [...new Set(towers.map(tower => tower.city))];

  const handleChange = (_, value) => {
    dispatch({type: 'update-cities', payload: value});
  }

  return (
    <StyledCitiesFilter
      size='small'
      multiple={true}
      fullWidth={true}
      limitTags={1}
      options={cities}
      disableClearable={true}
      value={state.cities}
      componentsProps={{ paper: { sx: { borderRadius: 5 } } }}
      onChange={handleChange}
      renderInput={
        params =>
          <TextField
            {...params}
            label='Select cities...'
          />
      }
    />
  );
}

export default CitiesFilter;