import { Card, CardContent, CardMedia, ButtonBase, Grid, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Link } from 'react-router-dom';
import { context } from '../../../../AppContext'
import { useContext } from 'react'

const StyledLink = styled(Link)({
  textDecoration: 'none',
});

const StyledCard = styled(Card)(({ theme }) => ({
  height: theme.spacing(35),
  borderRadius: theme.spacing(2),
}));

const StyledCardMedia = styled(CardMedia)(({ theme }) => ({
  height: theme.spacing(25),
}));

const FilterCard = (props) => {
  const { dispatch } = useContext(context)

  return (
    <Grid
      item={true}
      xs={12}
      md={6}
      lg={4}
      xl={3}
    >
      <StyledLink
        to='/tower'
      >
        <StyledCard>
          <div
            onClick={
              () =>
                dispatch({ type: 'update-tower', payload: { name: props.name, place: props.place, map: props.map } })
            }
          >
            <StyledCardMedia
              image={require('../../images/Towers/' + props.name + '.jpg')}
            />
            <CardContent>
              <Typography
                variant='h6'
              >
                {props.name}
              </Typography>
              <Grid
                container={true}
              >
                <Grid
                  item={true}
                  xs={9}
                >
                  <Typography
                    variant='body2'
                  >
                    {props.place}
                  </Typography>
                </Grid>
                <Grid
                  item={true}
                  xs={3}
                >
                  <Typography
                    variant='body2'
                    color={
                      props.status === 'Available' ? '#06c258' :
                        props.status === 'Unavailable' ? '#ff3333' :
                          '#000'
                    }
                  >
                    {props.status}
                  </Typography>
                </Grid>
              </Grid>
            </CardContent>
          </div>
        </StyledCard>
      </StyledLink>
    </Grid>
  )
}

export default FilterCard;