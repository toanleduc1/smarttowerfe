import { Grid } from '@mui/material';
import { styled } from '@mui/material/styles';
import { useContext } from 'react';

import towers from '../../data/towers.json';
import FilterCard from './FilterCard';
import { context } from './FilterContext';


const StyledFilterCardGroup = styled(Grid)(({ theme }) => ({
  paddingTop: theme.spacing(2),
  paddingBottom: theme.spacing(2),
  paddingLeft: theme.spacing(5),
  paddingRight: theme.spacing(5),
  [theme.breakpoints.up('sm')]: {
    paddingLeft: theme.spacing(20),
    paddingRight: theme.spacing(20),
  },
}));

const FilterCardGroup = () => {
  const { state } = useContext(context);

  const cards = towers.filter(
    tower =>
      state.cities.length === 0 ||
      (state.cities.includes(tower.city) && (state.districts.length === 0 || state.districts.includes(tower.district)))
  );

  return (
    <StyledFilterCardGroup
      container={true}
      spacing={3}
    >
      {
        cards.map((card, index) => (
          <FilterCard
            key={index}
            name={card.name}
            place={card.district + ', ' + card.city}
            status={card.status}
            map={card.map}
          />
        ))
      }
    </StyledFilterCardGroup>
  );
}

export default FilterCardGroup;