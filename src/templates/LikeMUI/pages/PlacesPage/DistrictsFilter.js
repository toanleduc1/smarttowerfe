import { Autocomplete, TextField } from '@mui/material';
import { styled } from '@mui/material/styles';
import { useContext } from 'react';

import towers from '../../data/towers.json';
import { context } from './FilterContext';


const StyledDistrictsFilter = styled(Autocomplete)(({ theme }) => ({
  '& label.Mui-focused': {
    color: '#fc3468', // pink
  },
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderRadius: theme.spacing(2),
    },
    '&.Mui-focused fieldset': {
      borderColor: '#fc3468', // pink
    },
  },
  marginTop: theme.spacing(1),
  marginBottom: theme.spacing(1),
  [theme.breakpoints.up('sm')]: {
    maxWidth: theme.spacing(28),
    marginInline: theme.spacing(1),
  },
}));


const DistrictsFilter = () => {
  const { state, dispatch } = useContext(context);
  const districts = [...new Set(
    towers.flatMap(
      tower =>
        state.cities.includes(tower.city) ? [tower.district] : []
    )
  )];

  const handleChange = (_, value) => {
    dispatch({ type: 'update-districts', payload: value });
  }

  return (
    <StyledDistrictsFilter
      size='small'
      multiple={true}
      fullWidth={true}
      limitTags={1}
      options={districts}
      disableClearable={true}
      value={state.districts}
      componentsProps={{ paper: { sx: { borderRadius: 5 } } }}
      onChange={handleChange}
      renderInput={
        params =>
          <TextField
            {...params}
            label='Select districts...'
          />
      }
    />
  );
}

export default DistrictsFilter;