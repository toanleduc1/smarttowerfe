import { Box, Button, ImageList, ImageListItem, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import AppsIcon from '@mui/icons-material/Apps';
import images from '../../data/images.json';
import Modal from '@mui/material/Modal';
import React from 'react';
import ModalImage from './ModalImage';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 'auto',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const StyledTowerImageGroup = styled(Box)({
  position:'relative',
  width: '98%',
});

const StyledTowerButton = styled(Button)({
  position:'absolute',
  bottom: '15px',
  right: '15px',
  opacity: 0.8,

});

const StyledTowerImage = styled('img')(({ theme }) => ({
  borderRadius: theme.spacing(2),
  [theme.breakpoints.down('lg')]: {
    width: '100%',
  },
}));

const TowerImageGroup = () => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  return (
    <StyledTowerImageGroup
      container={true}
      spacing={2}
    >
      <ImageList
        cols={images.length}
        gap={8}
      >
        {images.map((image, index) => (
          <ImageListItem
            key={index}
          >
            <StyledTowerImage
              src={`${image.path}?w=200&h=200&fit=crop&auto=format`}
              alt={image.title}
            />
          </ImageListItem>
        ))}
      </ImageList>
      <StyledTowerButton 
        variant="outlined"
        onClick={handleOpen}  
          >
        <AppsIcon />
      </StyledTowerButton>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <ModalImage image={images} />
        </Box>
      </Modal>
    </StyledTowerImageGroup>
  );
}

export default TowerImageGroup;