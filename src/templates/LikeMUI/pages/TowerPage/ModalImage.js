import * as React from 'react';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import Zoom from "react-medium-image-zoom";
import 'react-medium-image-zoom/dist/styles.css'

export default function ModalImage({image}) {
    console.log(image)
  return (
    <Box id="modal-box" sx={{ width: 800, height: 650, overflowY: 'scroll',  }}>
        <ImageList variant="masonry" cols={3} gap={8}>
            {image.map((item, index) => (
            <ImageListItem key={index}>
                {/* <img
                src={`${item.path}?w=248&fit=crop&auto=format`}
                // srcSet={`${item.path}?w=248&fit=crop&auto=format&dpr=2 2x`}
                alt={item.title}
                // loading="lazy"
                /> */}
                <Zoom>
                    <img
                        src= {`${item.path}?w=248&fit=crop&auto=format`}
                        alt= {item.title}
                        className= "img"
                        loading="lazy"
                        // width="500"
                    />
                </Zoom>
            </ImageListItem>
            ))}
        </ImageList>
    </Box>
  );
}