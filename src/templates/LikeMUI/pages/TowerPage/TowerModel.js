import React from 'react';
import { Suspense, useEffect } from 'react';
import { Canvas } from '@react-three/fiber';
import { useLoader } from '@react-three/fiber';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { Html, OrbitControls, useProgress } from '@react-three/drei';
import { styled } from '@mui/material/styles';
import { Paper, Typography } from '@mui/material';


const StyledTypography = styled(Typography)(({ theme }) => ({
  marginTop: theme.spacing(5),
}));

const StyledTowerModel = styled(Paper)(({ theme }) => ({
  width: '98%',
  height: theme.spacing(50),
}));

const Loader = () => {
  const { progress } = useProgress();
  return (
    <Html
      center
    >
      {progress}%
    </Html>
  );
}

const Model = () => {
  const gltf = useLoader(GLTFLoader, './model/scene.gltf');

  return (
    <primitive
      object={gltf.scene}
      scale={0.15}
      position={[0, 2, 0]}
    />
  );
};


const TowerModel = () => {
  return (
    <React.Fragment>
      <StyledTypography
        variant='h6'
      >
        Tower Model
      </StyledTypography>
      <StyledTowerModel>
        <Canvas
          style={{ background: 'black' }}
        >
          <Suspense
            fallback={<Loader />}
          >
            <Model />
            <OrbitControls />
          </Suspense>
        </Canvas>
      </StyledTowerModel>
    </React.Fragment>
  );
}

export default TowerModel;