import React, { useState } from 'react';
import ModalFile from './ModalFile';
import { Box, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Document, Page, pdfjs } from 'react-pdf';
import './TowerDocument.css';
import documents from '../../data/document.json';
import { DataGrid } from '@mui/x-data-grid';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';
import DownloadIcon from '@mui/icons-material/Download';
import Modal from '@mui/material/Modal';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const StyledTowerDocument = styled(Box)(({ theme }) => ({
  width: '98%',
  display: 'flex',
  flexDirection: 'column',
  maxHeight: theme.spacing(80),
  marginTop: theme.spacing(5),
  [theme.breakpoints.down('lg')]: {
    width: '100%',
    marginTop: theme.spacing(0),
  }
}));

function createData(id, title, path, size, type) {
  return {id, title, path, size, type};
}

const rows = 
  documents.map((doc, index) => 
    createData(doc.id, doc.title, doc.path, doc.size, doc.type)
  );

const TowerDocument = () => {
  const [open, setOpen] = React.useState(false);

  const handleOpen = (e) => {
    const idRow = e.target.parentNode.previousSibling.innerHTML
    console.log('idRow', idRow)
    console.log(documents)
    var RowWithID = documents.find(function (doc,index) {
      return doc.id === parseInt(idRow);
    });
    console.log("ktra", RowWithID)
    setRowClick(RowWithID)
    setOpen(true);
  }
  const handleClose = () => setOpen(false);
  const [rowClick, setRowClick] = React.useState(null)
  return (
    <StyledTowerDocument>
      <Typography
        variant='h6'
      >
        Technical document
      </Typography>
      <Box sx={{ height: '100%', border: '1px solid #ccc', padding: 'auto' }}>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell sx={{display:'none'}}>ID</TableCell>
                <TableCell>Title</TableCell>
                <TableCell align="right">Type</TableCell>
                <TableCell align="right">Size</TableCell>
                <TableCell align="right">Download</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row, index) => (
                <TableRow
                  key={index}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell sx={{display: 'none'}}>{row.id}</TableCell>
                  <TableCell component="th" scope="row">
                    {row.title}
                    {/* {row.type==='pdf' ? 
                      <Link href={row.path} underline="none">
                        {row.title}
                      </Link>
                      : 
                      <React.Fragment>
                        <p className="custom-title" onClick={handleOpen}>
                          {row.title}
                        </p>
                        <Modal
                          open={open}
                          onClose={handleClose}
                          aria-labelledby="modal-modal-title"
                          aria-describedby="modal-modal-description"
                        >
                          <ModalFile row={rowClick}/>
                        </Modal>
                      </React.Fragment>

                    } */}
                  </TableCell>
                  <TableCell align="right">{row.type}</TableCell>
                  <TableCell align="right">{row.size}</TableCell>
                  <TableCell align="right">
                    <Link href={row.path}>
                      <DownloadIcon />
                    </Link>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </StyledTowerDocument>
  );
}

export default TowerDocument;


{/* <StyledDocument
        title='document'
        src='https://docs.google.com/spreadsheets/d/e/2PACX-1vQ4iVWgD_PFYvPEbfQQQt9aqFH-6WhM6qT-5n2Of3WkcoukKTx8o7tJJ5JCNpu1dUShlqwQp33pkPBz/pubhtml?widget=true&amp;headers=false'
      /> */}
      
      
        {/* <Document 
          sx={{ height:'100%', overflow:'hidden'}} 
          file="./sample.pdf" 
          onLoadSuccess={onDocumentLoadSuccess}
          >
            <Page pageNumber={pageNumber} />
        </Document>
        <div>
          <p>
            Page {pageNumber || (numPages ? 1 : "--")} of {numPages || "--"}
          </p>
          <button className="button-direction" type="button" disabled={pageNumber <= 1} onClick={previousPage}>
            Previous
          </button>
          <button
            className="button-direction"
            type="button"
            disabled={pageNumber >= numPages}
            onClick={nextPage}
          >
            Next
          </button>
      </div> */}