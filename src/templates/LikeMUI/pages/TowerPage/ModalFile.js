import * as React from 'react';
import Box from '@mui/material/Box';
import { Typography } from '@mui/material';
import {OutTable, ExcelRenderer} from 'react-excel-renderer';
import FileViewer from 'react-file-viewer';

export default function ModalFile({row}) {
    const file = row.path
    const type = row.type
    // console.log('onModalFIle',row)

    function onError(e) {
        console.log('error in file-viewer');
    }
    return (
        <Box>
            <FileViewer
                fileType='pdf'
                filePath='./document/sample.pdf'
                onError={onError}
                />
            {console.log(type, file)}
        </Box>
    );
}