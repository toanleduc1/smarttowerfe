import React, { useContext } from 'react';
import { Grid, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';

import LMAppBar from '../../components/LMAppBar';
import TowerImageGroup from './TowerImageGroup';
import TowerMap from './TowerMap';
import TowerDocument from './TowerDocument';
import TowerPrice from './TowerPrice';
import TowerModel from './TowerModel';

import { context } from '../../../../AppContext';


const StyledTowerPage = styled(Grid)(({ theme }) => ({
  paddingTop: theme.spacing(10),
  paddingBottom: theme.spacing(2),
  paddingLeft: theme.spacing(5),
  paddingRight: theme.spacing(5),
  [theme.breakpoints.up('sm')]: {
    paddingLeft: theme.spacing(20),
    paddingRight: theme.spacing(20),
  },
}));

const StyledGrid = styled(Grid)(({ theme }) => ({
  [theme.breakpoints.up('lg')]: {
    position: 'fixed',
    right: '45px',
  },
  [theme.breakpoints.up('xl')]: {
    position: 'fixed',
    right: '65px',
  },
}));

const TowerPage = () => {
  const { state } = useContext(context)

  return (
    <React.Fragment>
      <LMAppBar />
      <StyledTowerPage
        container={true}
        spacing={2}
      >
        <Grid
          item={true}
          xs={12}
        >
          <Typography
            variant='h4'
          >
            {state.tower.name}
          </Typography>
          <Typography
            variant='subtitle1'
            sx={{ color: '#7e7e7e' }}
          >
            {state.tower.place}
          </Typography>
        </Grid>
        <Grid
          item={true}
          xs={12}
          lg={8}
        >
          <TowerImageGroup />
          <TowerDocument />
        </Grid>
        <StyledGrid
          item={true}
          xs={12}
          lg={4}
        >
            <TowerMap src={state.tower.map} />
            <TowerPrice />
        </StyledGrid>
        <Grid
          item={true}
          xs={12}
          lg={8}
        >
          <TowerModel />
        </Grid>
      </StyledTowerPage>
    </React.Fragment>
  );
}

export default TowerPage;