import { Button } from '@mui/material';
import { styled } from '@mui/material/styles';


const StyledTowerPriceButton = styled(Button)(({ theme }) => ({
  borderRadius: theme.spacing(1),
  textTransform: 'none',
  textDecoration: 'none',
  background: '#fc3468', // pink
  width: '100%',
  marginBottom: theme.spacing(2),
}));

const TowerPriceButton = () => {
  return (
    <StyledTowerPriceButton
      variant='contained'
      color='secondary'
    >
      Réservez
    </StyledTowerPriceButton>
  );
}

export default TowerPriceButton;