import { styled } from '@mui/material/styles';


const StyledTowerMap = styled('iframe')(({ theme }) => ({
  width: '100%',
  height: theme.spacing(70),
  marginTop: theme.spacing(2),
  borderRadius: theme.spacing(2),
  border: theme.spacing(0),
  [theme.breakpoints.down('lg')]: {
    marginTop: theme.spacing(5),
    height: theme.spacing(50),
  },
}));

const TowerMap = (props) => {
  return (
    <StyledTowerMap
      title='map'
      src={props.src}
    >
    </StyledTowerMap>
  );
}

export default TowerMap;