import { Card, Divider, Grid, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';

import TowerPriceButton from './TowerPriceButton';


const StyledTowerPrice = styled(Card)(({ theme }) => ({
  width: '70%',
  padding: theme.spacing(2),
  boxSizing: 'border-box', // padding without change size
  borderRadius: theme.spacing(2),
  marginTop: theme.spacing(2),
  [theme.breakpoints.down('lg')]: {
    width: '100%',
  },
}));

const TowerPrice = () => {
  return (
    <StyledTowerPrice>
      <TowerPriceButton />
      <Grid
        container={true}
        spacing={1}
      >
        <Grid
          item={true}
          xs={9}
        >
          <Typography
            variant='body2'
            align='left'
          >
            $1,000 x 3 months
          </Typography>
        </Grid>
        <Grid
          item={true}
          xs={3}
        >
          <Typography
            variant='body2'
            align='right'
          >
            €3,000
          </Typography>
        </Grid>
        <Grid
          item={true}
          xs={9}
        >
          <Typography
            variant='body2'
            align='left'
          >
            Discount
          </Typography>
        </Grid>
        <Grid
          item={true}
          xs={3}
        >
          <Typography
            variant='body2'
            align='right'
            color='#06c258'
          >
            €0
          </Typography>
        </Grid>
        <Grid
          item={true}
          xs={9}
        >
          <Typography
            variant='body2'
            align='left'
          >
            Note de calcul
          </Typography>
        </Grid>
        <Grid
          item={true}
          xs={3}
        >
          <Typography
            variant='body2'
            align='right'
          >
            €250
          </Typography>
        </Grid>
        <Grid
          item={true}
          xs={12}
        >
          <Divider />
        </Grid>
        <Grid
          item={true}
          xs={9}
        >
          <Typography
            variant='body2'
            align='left'
          >
            Total before taxes
          </Typography>
        </Grid>
        <Grid
          item={true}
          xs={3}
        >
          <Typography
            variant='body2'
            align='right'
          >
            €3,250
          </Typography>
        </Grid>
      </Grid>
    </StyledTowerPrice>
  );
}

export default TowerPrice;