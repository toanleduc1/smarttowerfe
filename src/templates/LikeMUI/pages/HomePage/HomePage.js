import React from 'react';

import LMAppBar from '../../components/LMAppBar';
import Preamble from './Preamble';
import Footer from './Footer';


const HomePage = () => {
  return (
    <React.Fragment>
      <LMAppBar />
      <Preamble />
      <Footer />
    </React.Fragment>
  );
}

export default HomePage;