import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';

import PreambleSearchButton from './PreambleSearchButton';
import PreambleLearnMoreButton from './PreambleLearnMoreButton';


const StyledPreambleButtonGroup = styled(Box)(({ theme }) => ({
  paddingTop: theme.spacing(2),
}));

const PreambleButtonGroup = () => {
  return (
    <StyledPreambleButtonGroup>
      <PreambleSearchButton />
      <PreambleLearnMoreButton />
    </StyledPreambleButtonGroup>
  );
}

export default PreambleButtonGroup;