import { Box } from '@mui/material';
import { styled } from '@mui/material/styles';

import PreambleTypography from './PreambleTypography';
import PreambleButtonGroup from './PreambleButtonGroup';


const StyledPreambleContent = styled(Box)({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  height: '100%',
});

const PreambleContent = () => {
  return (
    <StyledPreambleContent>
      <PreambleTypography />
      <PreambleButtonGroup />
    </StyledPreambleContent>
  );
}

export default PreambleContent;