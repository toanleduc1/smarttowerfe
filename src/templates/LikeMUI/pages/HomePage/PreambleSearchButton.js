import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import { Button } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Link } from 'react-router-dom';


const StyledPreambleSearchButton = styled(Button)(({ theme }) => ({
  flexGrow: 1,
  padding: theme.spacing(2),
  borderRadius: theme.spacing(2),
  textTransform: 'none',
  textDecoration: 'none',
  marginLeft: theme.spacing(1),
  marginRight: theme.spacing(1),
  background: '#fc3468', // pink
}));

const PreambleSearchButton = () => {
  return (
    <StyledPreambleSearchButton
      component={Link}
      to='/places'
      variant='contained'
      color='secondary'
      startIcon={<SearchOutlinedIcon />}
    >
      Get started
    </StyledPreambleSearchButton>
  );
}

export default PreambleSearchButton;