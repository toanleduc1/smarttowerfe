import { CardMedia } from '@mui/material';
import { styled } from '@mui/material/styles';

import preamble from '../../images/preamble.jpg';


const StyledPreambleImage = styled(CardMedia)({
  height: '100%',
});

const PreambleImage = () => {
  return (
    <StyledPreambleImage
      image={preamble}
    />
  );
}

export default PreambleImage;