import React from 'react';
import { Grid } from '@mui/material';
import { styled } from '@mui/material/styles';

import PreambleContent from './PreambleContent';
import PreambleImage from './PreambleImage';


const StyledPremable = styled(Grid)(({ theme }) => ({
  height: `calc(100vh - ${theme.mixins.toolbar.minHeight}px)`,
}));

const Preamble = () => {
  return (
    <React.Fragment>
      <StyledPremable
        container={true}
      >
        <Grid
          item={true}
          md={2}
        />
        <Grid
          item={true}
          xs={12}
          md={4}
        >
          <PreambleContent />
        </Grid>
        <Grid
          item={true}
          md={6}
        >
          <PreambleImage />
        </Grid>
      </StyledPremable>
    </React.Fragment>
  )
}

export default Preamble;