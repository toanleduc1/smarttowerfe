import ChevronRightOutlinedIcon from '@mui/icons-material/ChevronRightOutlined';
import { Button } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Link } from 'react-router-dom';


const StyledPreambleLearnMoreButton = styled(Button)(({ theme }) => ({
  flexGrow: 1,
  padding: theme.spacing(2),
  borderRadius: theme.spacing(2),
  textTransform: 'none',
  textDecoration: 'none',
  marginLeft: theme.spacing(1),
  marginRight: theme.spacing(1),
}));

const PreambleLearnMoreButton = () => {
  return (
    <StyledPreambleLearnMoreButton
      component={Link}
      to='/technical'
      variant='outlined'
      color='inherit'
      endIcon={<ChevronRightOutlinedIcon />}
    >
      Learn more
    </StyledPreambleLearnMoreButton>
  );
}

export default PreambleLearnMoreButton;