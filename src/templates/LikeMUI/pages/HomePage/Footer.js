import { Box, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';


const StyledFooter = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  boxShadow: '0px -10px 10px -10px rgba(0,0,0,.99)',
  height: theme.spacing(8),
  marginTop: theme.spacing(1),
  [theme.breakpoints.down('sm')]: {
    display: 'none',
  }
}));

const Footer = () => {
  return (
    <StyledFooter>
      <Box>
        <Typography
          variant='h6'
        >
          Copyright © 2022, Smart Tower. All rights reserved.
        </Typography>
      </Box>
    </StyledFooter>
  );
}

export default Footer;