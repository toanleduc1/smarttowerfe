import { Typography } from '@mui/material';
import { styled } from '@mui/material/styles';


const StyledPreambleTypography = styled(Typography)(({ theme }) => ({
  paddingLeft: theme.spacing(5),
}));

const PreambleTypography = () => {
  return (
    <StyledPreambleTypography
      variant='h1'
    >
      Quickly rent a radio location
    </StyledPreambleTypography>
  );
}

export default PreambleTypography;