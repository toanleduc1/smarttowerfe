import * as React from 'react';
import { useState } from 'react';
import './LoginPage.css'
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import AccountCircle from '@material-ui/icons/AccountCircle';
import LockRoundedIcon from '@material-ui/icons/LockRounded';
import Button from '@mui/material/Button';
import SaveIcon from '@mui/icons-material/Save';
import LoadingButton from '@mui/lab/LoadingButton';
import Typography from '@mui/material/Typography';

import { useContext } from 'react';
import { store } from '../../store';
import { useNavigate } from 'react-router-dom';


const fakeLoginRequest = (username, password) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if(username === 'admin' && password === 'admin') {
        resolve({'token': 'admin-access-token'});
      } else {
        reject({'error': 'Username or password is incorrect'})
      }
    }, 1000);
  });
}

export default function LoginPage(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const { globalDispatch } = useContext(store);
  const navigate = useNavigate();

  const handleUserNameChange = (e) => {
    setMessage(' ');
    setUsername(e.target.value);
  }

  const handlePasswordChange = (e) => {
    setMessage(' ');
    setPassword(e.target.value);
  }

  const handleLogin = () => {
    setLoading(true);
    fakeLoginRequest(username, password).then((data) => {
      globalDispatch({type: 'update-user', payload: {status: 'logged-in'}, data: data});
      navigate('/home');
    }).catch(() => {
      setLoading(false);
      setMessage('Username or password is incorrect');
    }); 
  }

  return (
    <div id='background'>
      <Box id='form'>
        <Box sx={{ '& > :not(style)': { m: 1 } }}>
          <h1>Login to SmartTowers</h1>
          <Box sx={{ display: 'flex', alignItems: 'flex-end'}}>
            <AccountCircle />
            <TextField 
              id="input-with-sx" 
              label="Username" 
              variant="standard"
              onChange={handleUserNameChange}
              />
          </Box>
          <Box sx={{ display: 'flex', alignItems: 'flex-end'}}>
            <LockRoundedIcon />
            <TextField 
              id="input-with-sx" 
              type="password" 
              label="Password" 
              variant="standard"
              onChange={handlePasswordChange}
              />
          </Box>
          <Box>
            {
                loading===false ?
                <Button 
                // style={{ width: '210px'}}
                className='buttonSignIn'
                variant="contained"
                onClick={handleLogin}
                >
                    LOGIN
                </Button>
                :
                <LoadingButton
                    style={{ color: 'white'}}
                    className='buttonSignIn'
                    loading
                    loadingPosition="start"
                    startIcon={<SaveIcon />}
                    variant="outlined"
                    onClick={handleLogin}
                >
                    LOGIN
                </LoadingButton>
            }
        </Box>
          {/* {loading ? <CircularProgress /> : <h4>{message}</h4>} */}
        {loading ? <Typography className="forgotPass" variant="p" gutterBottom component="div">
                    
        </Typography> : 
        <Typography style={{ color: 'red'}} className="forgotPass" variant="p" gutterBottom component="div">
            {message}
        </Typography> }
        </Box>
      </Box>
    </div>
  );
}
