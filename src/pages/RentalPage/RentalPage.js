import React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TopBar from '../SitePage/TopBar'
import { makeStyles } from "@material-ui/core/styles";
import DialogIndoor from './DialogIndoor';
import DialogOutdoor from './DialogOutdoor';


const useStyles = makeStyles(theme => ({
    card: {
        // position: "relative",
        background: 'red'
    },
    font: {
        // position: "absolute",
        top: "20%",
        width: "100%",
        // textAlign: "center",
        color: "white",
        backgroundColor: "none",
        fontFamily: "Comic Sans MS!important"
    }


}));

function RentalPage(props) {
    const classes = useStyles();
    return (
        <Box>
            <TopBar />
            <Grid container>
                <Grid xs={6}
                    style={{ background: '#8A2BE2', height: '100vh' }}
                >
                    <Typography
                        style={{ marginLeft: '30px', marginTop: '40vh', color: 'white' }}
                        variant="h3"
                        component="h3"
                    >
                        What tower will you rent?
                    </Typography>
                </Grid>
                <Grid xs={6} style={{ margin: 'auto' }}>
                    <Box sx={{ padding: 1 }}>
                        <DialogIndoor />
                    </Box>
                    <Box sx={{ padding: 1 }}>
                        <DialogOutdoor />
                    </Box>
                </Grid>
            </Grid>
        </Box>
    );
}

export default RentalPage;