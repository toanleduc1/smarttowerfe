import React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import CardMedia from '@mui/material/CardMedia';
import btsindoor from '../../images/btsindoor.png'
import Checkbox from '@mui/material/Checkbox';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';



function DialogIndoor(props) {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

    const data = [
        { name: 'item1', checked: false },
        { name: 'item2', checked: false },
        { name: 'item3', checked: true },
        { name: 'item4', checked: false },
    ]

    const checkedData = []

    data.forEach((item, index) => {
        if (item.checked === true) {
            checkedData.push(index)
        }
    })
        
    const [checked, setChecked] = React.useState(checkedData);

    const handleToggle = (value) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];
        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }
        setChecked(newChecked);
    };

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleCancel = () => {
        setOpen(false);
        setChecked(checkedData)
    };
    const handleSave = () => {
        setOpen(false)
        checked.forEach((item) => {
            data[item].checked = true
        })
    }

    return (
        <div>
            <Button variant="outlined" fullWidth onClick={handleClickOpen}>
                <Grid xs={9}>
                    <CardContent>
                        <Typography component="div" variant="h6" align='left'>
                            Indoor
                        </Typography>
                    </CardContent>

                </Grid>
                <Grid xs={3}>
                    <CardMedia
                        component="img"
                        sx={{ width: 151 }}
                        image={btsindoor}
                        alt="Indoor"
                    />
                </Grid>
            </Button>
            <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={handleCancel}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">
                    {"Indoor"}
                </DialogTitle>
                <DialogContent>
                    <List dense sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                        {data.map((value, index) => {
                            const labelId = `checkbox-list-secondary-label-${value.name}`;
                            return (
                                <ListItem
                                    key={value.name}
                                    secondaryAction={
                                        <Checkbox
                                            onChange={handleToggle(index)}
                                            checked={checked.indexOf(index) !== -1}
                                            inputProps={{ 'aria-labelledby': labelId }}
                                        />
                                    }
                                >
                                    <ListItemButton>
                                        <ListItemText id={labelId} primary={value.name} />
                                    </ListItemButton>
                                </ListItem>
                            );
                        })}
                    </List>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleCancel}>
                        Cancel
                    </Button>
                    <Button onClick={handleSave} autoFocus>
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default DialogIndoor;