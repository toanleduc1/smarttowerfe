import React from 'react';
import { OutTable, ExcelRenderer } from 'react-excel-renderer';
// import Card from '@mui/material/Card';
import { Jumbotron, Col, Input, InputGroup, InputGroupAddon, FormGroup, Label, Button, Fade, FormFeedback, Container, Card } from 'reactstrap';
import './TechDocument.css'
import fileExcel from '../../data/Dịch Pháp_Bieu mau mo ta thong tin tram vien thong.xlsx'


function TechDocument(props) {
    const [rows, setRows] = React.useState([])
    const [cols, setCols] = React.useState([])
    const fileHandler = (event) => {
        let fileObj = event.target.files[0];
        console.log(fileObj)
        ExcelRenderer(fileObj, (err, resp) => {
            if (err) {
                console.log(err);
            }
            else {
                setRows(resp.rows)
                setCols(resp.cols)
            }
        });
    }


    return (
        <div>
            <input type="file" onChange={fileHandler} style={{ "padding": "10px" }} />
            <Container>
                <div>
                    <Card body outline color="secondary" className="restrict-card">
                        <OutTable data={rows} columns={cols} tableClassName="ExcelTable2007" tableHeaderRowClass="heading" />
                    </Card>
                </div>
            </Container>
        </div >
    );
}

export default TechDocument;