import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import TopBar from '../SitePage/TopBar';
import BasicTabs from './BasicTabs';


export default function SelectionPage() {
  return (
    <Box>
      <TopBar />
      <Grid container className={"GridContainer"} padding={2}>
        <BasicTabs />
      </Grid>
    </Box>
  );
}