import React, { Component } from 'react';
// import ReactDOM from 'react-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import tower from '../../images/tower.jpg';

export default class DemoCarousel extends Component {
  render() {
    return (
      <Carousel>
        <div>
          <img src={tower} alt='tower1' />
          {/* <p className="legend">Legend 1</p> */}
        </div>
        <div>
          <img src={tower} alt='tower2' />
          {/* <p className="legend">Legend 2</p> */}
        </div>
        <div>
          <img src={tower} alt='tower3' />
          {/* <p className="legend">Legend 3</p> */}
        </div>
      </Carousel>
    );
  }
}