import Grid from '@mui/material/Grid';
import SimpleMap from './SimpleMap';
import Paper from '@mui/material/Paper';
import DemoCarousel from './DemoCarousel';


export default function SummaryTab() {
  return (
    <Grid container spacing={2} className={"GridContainer"} padding={2}>
      <Grid item xs={6}>
        <Paper><SimpleMap /></Paper>
      </Grid>
      <Grid item xs={6}>
        <DemoCarousel />
      </Grid>
    </Grid>
  );
}