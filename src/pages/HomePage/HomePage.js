import React from 'react';
import Box from '@mui/material/Box';
import TopBar from '../SitePage/TopBar';
import SearchBar from '../SitePage/SearchBar';
import CardMedia from '@mui/material/CardMedia';
import towerbts from '../../images/towerbts.jpeg';
import engineer from '../../images/engineer.jpeg';
import Card from '@mui/material/Card';
import { Button, CardActionArea } from '@mui/material';
import Typography from '@mui/material/Typography';
import CardContent from '@mui/material/CardContent';
import marseile from '../../images/Marseille.jpeg'
import paris from '../../images/Paris.jpeg'
import toulouse from '../../images/Toulouse.jpeg'
import lyon from '../../images/Lyon.jpeg'
import Grid from '@mui/material/Grid';
import { makeStyles } from "@material-ui/core/styles";

const data = [
    { name: 'Marseille', count: 13, image: marseile },
    { name: 'Paris', count: 12, image: paris },
    { name: 'Toulouse', count: 31, image: toulouse },
    { name: 'Lyon', count: 69, image: lyon },
]

const useStyles = makeStyles(theme => ({
    card: {
        // position: "relative"
    },
    font: {
        position: "absolute",
        top: "50%",
        // width: "100%",
        // textAlign: "center",
        color: "white",
        backgroundColor: "none",
        fontFamily: "Comic Sans MS!important"
    },
    chat: {
        position: "absolute!important",
        top: '70%',
        left: '10%',
    }
}));

function HomePage(props) {
    const classes = useStyles();
    return (
        <Box>
            <TopBar />
            <SearchBar />
            <Card style={{ padding: 40, marginTop: 10 }} className={classes.card} >
                <CardMedia
                    component="img"
                    height="700"
                    image={towerbts}
                    alt="tower"
                />
                <Typography
                    gutterBottom
                    variant="h3"
                    component="h3"
                    className={classes.font}
                >
                    Trouvez facilement votre emplacement.
                </Typography>
            </Card>
            <Box style={{ padding: 40 }} >
                <h1>Vos dernières recherches</h1>
                <Grid container>
                    {
                        data.map(item => (
                            <Grid xs={3} style={{ padding: 15 }}>
                                <Card sx={{ maxWidth: 345, borderRadius: 10 }}>
                                    <CardActionArea>
                                        <CardMedia
                                            component="img"
                                            height="140"
                                            image={item.image}
                                            alt="green iguana"
                                        />
                                        <CardContent>
                                            <Typography gutterBottom variant="h5" component="div">
                                                {item.name} {item.count}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                            </Grid>
                        ))
                    }
                </Grid>
            </Box>
            <Card
                style={{ paddingBottom: 20, position: 'absolute', width: '100%' }}
            >
                <CardMedia
                    component="img"
                    height="700"
                    image={engineer}
                    alt="tower"
                />
                <Typography
                    gutterBottom
                    variant="h3"
                    component="h3"
                    style={{position:'absolute', top: '20%', color: 'white'}}
                >
                    Des questions sur l’utilisation ?
                </Typography>
                <Button
                    variant="contained"
                    color='inherit'
                    className={classes.chat}
                >
                    Chat with a SuperAdmin
                </Button>
            </Card>
        </Box>
    );
}

export default HomePage;