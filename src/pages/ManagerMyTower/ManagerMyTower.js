import React from "react";
import { Box } from "@mui/material";
import TopBar from "../SitePage/TopBar";
import Pagination from '@mui/material/Pagination';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import MediaCard from "../SitePage/MediaCard";
import Typography from '@mui/material/Typography';
import SearchBar from "../SitePage/SearchBar";

import { useContext } from 'react';
import { store } from '../../store';

export default function ManagerMyTower(){
  const { globalState } = useContext(store);
//   const userType = 'host'
  const userType = 'custommer'


    return (
        <Box>
            {userType==='host' &&

                <Box>
                    <TopBar />
                    <Box
                        sx={{
                            width: '100%',
                            height: 60,
                            backgroundColor: 'primary.dark',
                            '&:hover': {
                            backgroundColor: 'primary.main',
                            opacity: [0.9, 0.8, 0.7],
                            }
                        }}
                        >
                        <Typography variant="h3" gutterBottom component="div" ml={2}>
                            All your tower
                        </Typography>
                    </Box>
                    <SearchBar />
                    <Grid container spacing={2} className={"GridContainer"} padding={2}>
                        <Grid item xs={12}>
                            <Grid container spacing={3}>
                                {
                                globalState.search.data.map((tower, index) => (
                                    <Grid item key={index} xs={2}>
                                        <MediaCard
                                            {...tower}
                                        />
                                    </Grid>
                                ))
                                }
                            </Grid>
                        </Grid>
                    </Grid>
                </Box>
                }

            {userType==='custommer' &&

            <Box>
                <TopBar />
                <Box
                    sx={{
                        width: '100%',
                        height: 60,
                        backgroundColor: 'primary.dark',
                        '&:hover': {
                        backgroundColor: 'primary.main',
                        opacity: [0.9, 0.8, 0.7],
                        }
                    }}
                    >
                    <Typography variant="h3" gutterBottom component="div" ml={2}>
                        All your tower
                    </Typography>
                </Box>
                <SearchBar />
                <Grid container spacing={2} className={"GridContainer"} padding={2}>
                    <Grid item xs={12}>
                        <Grid container spacing={3}>
                            {
                            globalState.search.data.map((tower, index) => (
                                <Grid item key={index} xs={2}>
                                    <MediaCard
                                        {...tower}
                                    />
                                </Grid>
                            ))
                            }
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
            }
            
           

            
        </Box>
    )
}