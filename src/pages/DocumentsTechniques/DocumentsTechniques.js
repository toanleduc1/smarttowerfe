import { Box, height } from "@mui/system";
import React from "react";
import TopBar from "../SitePage/TopBar";
import Typography from '@mui/material/Typography';
import SearchField from "react-search-field";
import Grid from '@mui/material/Grid';
import ResultBox from './ResultBox'
import DocumentsCarousel from "./DocumentCarousel";
import './ResultBox.css'
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Autocomplete from '@mui/material/Autocomplete';
import MuiAlert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

function fakeAPIListEquipment() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const listEquipment = [
                {index: 1,name: 'Huawei HSE4G 1', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', anten: '4T4R and 8T8R'},
                {index: 2,name: 'Huawei HSE4G 2', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', anten: '4T4R and 8T8R'},
                {index: 3,name: 'Huawei HSE4G 3', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', anten: '4T4R and 8T8R'},
                {index: 4,name: 'Huawei HSE4G 4', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', anten: '4T4R and 8T8R'},
                {index: 4,name: 'Huawei HSE4G 5', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', anten: '4T4R and 8T8R'},
                {index: 4,name: 'Huawei HSE4G 6', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', anten: '4T4R and 8T8R'},
                {index: 4,name: 'Huawei HSE4G 7', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', anten: '4T4R and 8T8R'},
                {index: 4,name: 'Huawei HSE4G 8', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', anten: '4T4R and 8T8R'},
                {index: 4,name: 'Huawei HSE4G 9', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', anten: '4T4R and 8T8R'},
            ]
            resolve(listEquipment);
        }, 1000);
      });
}

export default function DocumentsTechniques(props){
    const [open, setOpen] = React.useState(false);
    const [eqipClick, setEquipClick] = React.useState('')
    const [equip, setEquip] = React.useState([])
    React.useEffect(() => {
        fakeAPIListEquipment().then(data => {
            setEquip(data)
        })  
    },[])

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
      };

    function handleEnterSearch(event) {
        if (event.key === 'Enter') {
            
          // Prevent's default 'Enter' behavior.
            event.defaultMuiPrevented = true;
          // your handler code
            console.log(event.target.value)
            
            const x = equip.find(element=> element.name===event.target.value)
            if(x === undefined){
                setOpen(true);
            }else{
                setEquipClick(event.target.value)
                const elementFocus = document.getElementsByClassName('focus')
                // console.log(elementFocus)

            }
        }
    }

    return (
        <Box sx={{ width: '100%', height: '100vh', flexGrow: 1 }}>
            <TopBar />
            <Grid container spacing={2} sx={{ width: '100%', height: '92vh'}}>
                <Grid item xs={5} sx={{ ml:3, mt: 7}}>
                    <Typography variant="h7" gutterBottom component="div" sx={{ mt: 5}}>
                        TDF – Terrasse 100 rue leo - Paris 75001 -  Site N°1234567
                    </Typography>
                    <Box id="box-search">
                        <Autocomplete
                            id="free-solo-demo"
                            freeSolo
                            options={equip.map((e) => e.name)}
                            renderInput={(params) => <TextField {...params} label="Search material here" />}
                            onKeyDown={handleEnterSearch}
                        />
                    </Box>
                    <ResultBox eClick={eqipClick} setEClick={setEquipClick} eList={equip} setEList={setEquip}/>

                </Grid>
                <Grid item xs={6} className="slideDoc">
                    <DocumentsCarousel eclick={eqipClick} sx={{mt:5}}/>
                </Grid>

            </Grid>
            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Equipment cannot be found\n Please try again!
                </Alert>
            </Snackbar>

            
        </Box>
    )
}