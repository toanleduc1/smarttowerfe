import { render } from "@testing-library/react";
import React from "react";
import { Box } from "@mui/material";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import Typography from "@mui/material/Typography";


export default function DocumentsCarousel(props){

    const [listDoc, setlistDoc]= React.useState([])
    console.log("carou", props.eclick)
    React.useEffect(()=>{
        if(props.eclick===''){
            setlistDoc([])
        }else
        if(props.eclick==='Huawei HSE4G 1'){
            setlistDoc([
                { index: 1, town: 'Huwei', image:'./download.png'},
                { index: 2, town: 'Huwei', image:'./download.png'},
                { index: 3, town: 'Huwei', image:'./download.png'},
                { index: 4, town: 'Huwei', image:'./download.png'},
            ])
        }else{
            setlistDoc([
                { index: 1, town: 'Huwei', image:'./doc.png'},
                { index: 2, town: 'Huwei', image:'./doc.png'},
                { index: 3, town: 'Huwei', image:'./doc.png'},
                { index: 4, town: 'Huwei', image:'./doc.png'},
                // https://4ww1y37tl91gmoej12r01u1c-wpengine.netdna-ssl.com/wp-content/uploads/2019/08/TextDocument.png,
                // https://4ww1y37tl91gmoej12r01u1c-wpengine.netdna-ssl.com/wp-content/uploads/2019/08/Grayscale.jpg,
                // https://4ww1y37tl91gmoej12r01u1c-wpengine.netdna-ssl.com/wp-content/uploads/2019/08/Color.jpg
        
            ])
        }
    }, [props.eclick])
    return (
        <Box sx={{ mt: 2}}>
            {props.eclick==='' ? 
                (
                    <Box sx={{ width:'100%', height:'80vh', backgroundColor:'#FA8072'}}>
                        <Typography variant="h1" gutterBottom component="div" sx={{ lineHeight: 2, p:4}}>
                            Choose a equipment to show document!
                        </Typography>
                    </Box>
                ) :
            <Carousel>
                {listDoc.map(item => (
                    <div key={item.index}>
                        <img src={item.image} alt="Slide Document" style={{height: '100%'}}/>
                    </div>
                ))}
            </Carousel>
            }
            
        </Box>
    )
}