import React, { Fragment } from "react";
import { Box } from "@mui/system";
import { Typography, List, ListItem, Divider, ListItemText } from "@mui/material";
import './ResultBox.css'
import FileDownloadIcon from '@mui/icons-material/FileDownload';


export default function ResultBox(props){
    console.log('props', props)
    function handleClickEquip(e){
        props.setEClick(e.target.innerText)
    }

    return (
        <Box id='result-box'>
            <Typography className='result-box-title' variant='h6' component="div">
                Résultat de la recherche:
            </Typography>
            <Box className='result-box-list'>
                <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
                    {
                        props.eList.map((item, index) => (
                            <Box key={index} class="result-box-list_subbox">
                                <Box sx={{ mb:1}}>
                                    <Typography 
                                        className={item.name === props.eClick ? 'title-equip focus' : 'title-equip'}
                                        variant='h5' 
                                        display="inline-block"
                                        onClick={handleClickEquip}
                                        >
                                        {item.name}
                                    </Typography>
                                    {item.name === props.eClick ? <FileDownloadIcon sx={{ color: 'blue'}}/> : ''}
                                    <Typography className='detail-equip' variant='h7' display="inline-block">
                                        {item.frequency1}, {item.frequency2}, {item.anten}
                                    </Typography>
                                </Box>
                                <Divider/>
                            </Box>
                        ))
                    }
                </List>
            </Box>
        </Box>
    )
}