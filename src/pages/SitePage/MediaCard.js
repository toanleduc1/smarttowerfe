import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { ButtonBase } from '@mui/material';

import tower from '../../images/tower.jpg';
import { store } from '../../store';
import { useNavigate } from 'react-router-dom';


export default function MediaCard(props) {
  const { globalDispatch } = React.useContext(store);
  const navigate = useNavigate();
  
  const handleCardClick = () => {
    globalDispatch({ type: 'update-selection', payload: {...props} });
    navigate('/selection');
  }


  return (
    <ButtonBase onClick={handleCardClick}>
      <Card sx={{ maxWidth: 345 }}>
        <CardMedia
          component="img"
          height="140"
          image={tower}
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {props.name}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {props.detail}
          </Typography>
        </CardContent>
      </Card>
    </ButtonBase>
  );
}