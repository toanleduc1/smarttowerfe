import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import TopBar from './TopBar';
import SearchBar from './SearchBar';
import MediaCard from "./MediaCard";
import Pagination from '@mui/material/Pagination';
import SimpleMap from './SimpleMap';
import Paper from '@mui/material/Paper';

import { useContext } from 'react';
import { store } from '../../store';


export default function SitePage() {
  const { globalState } = useContext(store);

  return (
    <Box>
      <TopBar />
      <SearchBar />
      <Grid container spacing={2} className={"GridContainer"} padding={2}>
        <Grid item xs={6}>
          <Grid container spacing={2}>
            {
              globalState.search.data.map((tower, index) => (
                <Grid item key={index} xs={4}>
                  <MediaCard
                    {...tower}
                  />
                </Grid>
              ))
            }
            {
              globalState.search.data.length !== 0 &&
              <Grid item xs={12} display="flex" justifyContent="center" alignItems="center">
                <Pagination count={Math.ceil(globalState.search.data.length / 6)} />
              </Grid>
            }
          </Grid>
        </Grid>
        <Grid item xs={6}>
          <Paper><SimpleMap /></Paper>
        </Grid>
      </Grid>
    </Box>
  );
}