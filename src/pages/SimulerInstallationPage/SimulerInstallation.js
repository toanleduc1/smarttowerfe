import * as React from 'react';
import Typography from '@mui/material/Typography';
import { Button, Box } from '@mui/material';
import Town3D from './Town3D';
import SearchField from "react-search-field";
import './SimulerInstallation.css'
import ResultBox from './ResultBox';
import PegManDirection from './PegmanDirection';



export default function SimulerInstallation(props){

    function handleValidateEtude(){
        alert('etude validated')
    }
    
    return (
        <Box sx={{ width: '100%', height: '80vh' }}>

            {/* background Town 3D */}
            <Town3D />
            
            {/* search */}
            <Typography className="title-tower" variant="h7" gutterBottom component="div">
                TDF – Terrasse 100 rue leo - Paris 75001 -  Site N°1234567
            </Typography>
            <Box className="validate-etude">
                <Button 
                    variant="contained"
                    onClick={handleValidateEtude}
                    >
                        Valider l’étude
                    </Button>
            </Box>

            
            <SearchField
                placeholder="Search..."
                // onChange={onChange}
                // searchText="This is initial search text"
                classNames="search-material"
            /> 

            {/* result search */}
            <ResultBox />

            {/* pegman */}
            <Box id='pegman'>
                <PegManDirection />
                
            </Box>

            {/* compass */}
            <Box id='Compass'>
                {/* <CompassTown /> */}
            </Box>

        </Box>
    )
}