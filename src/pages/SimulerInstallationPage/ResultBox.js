import React, { Fragment } from "react";
import { Box } from "@mui/system";
import { Typography, List, ListItem, Divider, ListItemText } from "@mui/material";
import "./SimulerInstallation.css"


function fakeAPIListEquipment() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const listEquipment = [
                {name: 'Huawei HSE4G', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', dontKnow: '4T4R and 8T8R'},
                {name: 'Huawei HSE4G', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', dontKnow: '4T4R and 8T8R'},
                {name: 'Huawei HSE4G', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', dontKnow: '4T4R and 8T8R'},
                {name: 'Huawei HSE4G', frequency1 : '450–6000MHz', frequency2: '24250–52600MHz', dontKnow: '4T4R and 8T8R'},
            ]
            resolve(listEquipment);
        }, 1000);
      });
}


export default function ResultBox(props){

    const [equip, setEquip] = React.useState([])

    React.useEffect(() => {
        fakeAPIListEquipment().then(data => {
            setEquip(data)
        })  
    },[])
    return (
        <Box id='result-box'>
            <Typography className='result-box-title' variant='h7' component="div">
                Résultat de la recherche:
            </Typography>
            <Box className='result-box-list'>
                <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
                    {
                        equip.map((item, index) => (
                            <Fragment key={index}>

                                <ListItem alignItems="flex-start"> 
                                    <ListItemText
                                    primary={item.name}
                                    secondary={
                                        <React.Fragment>
                                            {item.frequency1}, {item.frequency2}, {item.dontKnow}
                                        </React.Fragment>
                                    }
                                    />
                                </ListItem>
                                <Divider/>
                            </Fragment>
                        ))
                    }
                </List>
            </Box>
        </Box>
    )
}