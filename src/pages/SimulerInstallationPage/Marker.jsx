import React from "react";
import './SimulerInstallation.css'
import RoomIcon from '@mui/icons-material/Room';

const MyMarker = ( props) => {
  

  return (
    <div >
      <RoomIcon className="marker" />
    </div>
  );
};

export default MyMarker;