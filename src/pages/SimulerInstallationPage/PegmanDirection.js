import React from 'react';
import GoogleMapReact from 'google-map-react';
import { Button, Box, Icon } from '@mui/material';
import MyMarker from './Marker';

export default function PegManDirection(props){

  const center = { lat: 21, lng: 105 }
  const zoom = 13

  return (
    <div id='map' style={{ height: '100%', width: '100%'}}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: 'AIzaSyAv1V90s2Ho36V0hsq5yI0zR4gy2f5ZpRE' }}
        defaultCenter={center}
        defaultZoom={zoom}
      >
        <MyMarker lat={center.lat} lng={center.lng} image="./East.png" />
      </GoogleMapReact>
    </div>
  );
}
