import React from "react";
import { Box } from "@mui/system";
import { Canvas } from "@react-three/fiber";
import { useLoader } from "@react-three/fiber";
import { Environment, OrbitControls, Cloud, Sky } from "@react-three/drei";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { Suspense } from "react";
import { Button } from "@mui/material";
import * as THREE from 'three'
import { TransformControls } from 'three/examples/jsm/controls/TransformControls'
// import "./styles.css";

export default function Town3D(props) {

const camera = new THREE.PerspectiveCamera(
  50,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
)
camera.position.z = 2

const renderer = new THREE.WebGLRenderer()
renderer.setSize(window.innerWidth, window.innerHeight)

  const controls = new TransformControls(camera, renderer.domElement)

  const y = React.useRef('')
  const z = React.useRef('')


  const x = new THREE.Object3D().rotateX(3)
  const Model = () => {
    const gltf = useLoader(GLTFLoader, "./model/scene.gltf");
    controls.attach(gltf)
    return (
      <>  
        <primitive object={gltf.scene} scale={0.1}  ref={y} position={[0,1,0]} onMouseDown={handleRotate}/>
      </>
    );
  }

  function handleRotate(){
    console.log(controls.mode)
    if(controls.mode === "rotate") {
      console.log("transform")
    }
  }


  controls.addEventListener('mouseDown', handleRotate)

  
  function handle(){
    console.log(controls)
    // console.log(x)
    // console.log(y.current)
    // console.log(y.current.children.rotation)
    y.current.rotation.y += 0.2
    // console.log(z)
    if(controls.mode === "rotate") {
      console.log("transform")
    }
  }

  return (
    <Box sx={{ width: '100%', height: '100%' }} id="town-3d">

      <Canvas>
        <Suspense fallback={null}>
          <Model/>
          <OrbitControls ref={z}/>
            <Sky distance={450000} sunPosition={[0, 1, 0]} inclination={0} azimuth={0.25} {...props} />
          {/* <Environment preset="sunset" background/> // background props if need */}
        </Suspense>
      </Canvas>
      <Button variant="contained" onClick={handle} />
    </Box>
  );
};
