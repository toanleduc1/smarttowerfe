import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import english from './translations/en.json';
import vietnamese from './translations/vi.json';

i18n
  .use(initReactI18next)
  .init({
    resources: {
      en: {
        translation: english
      },
      vi: {
        translation: vietnamese
      },
    },
    lng: 'en',
  });