import { createContext, useReducer } from 'react';


const init = {
  tower: {},
}

const context = createContext(init);

const AppProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'update-tower':
        return { tower: action.payload };
      default:
        return state;
    }
  }, init);

  return (
    <context.Provider
      value={{ state, dispatch }}
    >
      {children}
    </context.Provider>
  );
}

export { context, AppProvider };