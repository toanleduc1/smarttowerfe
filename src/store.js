import { createContext, useReducer } from 'react';
import { locations } from './pages/SitePage/search.data';


const initialState = {
  user: { status: 'not-login', data: {} },
  search: { status: '', data: locations },
  tower: {status: '', data: {}},
};

const store = createContext(initialState);

const StateProvider = ({ children }) => {
  const [globalState, globalDispatch] = useReducer(
    (state, action) => {
      switch (action.type) {
        case 'update-user':
          return { ...state, user: action.payload };
        case 'logout':
          return { ...state, user: initialState.user };
        case 'update-search':
          console.log(state);
          return { ...state, search: action.payload };
        default:
          return state;
      };
    }, initialState
  );

  return (
    <store.Provider
      value={{ globalState, globalDispatch }}
    >
      {children}
    </store.Provider>
  );
};

export { store, StateProvider };