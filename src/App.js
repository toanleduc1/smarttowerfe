// import logo from './logo.svg';
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import './App.css';
import SitePage from './pages/SitePage';
import SelectionPage from './pages/SelectionPage';
import { StateProvider } from './store';
import HomePage from './templates/LikeMUI/pages/HomePage';
import LoginPage from './pages/LoginPage/LoginPage';
import SimulerInstallation from './pages/SimulerInstallationPage/SimulerInstallation';
import RentalPage from './pages/RentalPage/RentalPage';
import PlacesPage from './templates/LikeMUI/pages/PlacesPage/PlacesPage';
import TowerPage from './templates/LikeMUI/pages/TowerPage/TowerPage';
import ManagerMyTower from "./pages/ManagerMyTower/ManagerMyTower";
import DocumentsTechniques from "./pages/DocumentsTechniques/DocumentsTechniques";
import { AppProvider } from "./AppContext";


function App() {
  return (
    <BrowserRouter>
      <StateProvider>
        <AppProvider>
          <Routes>
            <Route path="/" element={<LoginPage />} />
            <Route path="/home" element={<HomePage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/installation" element={<SimulerInstallation />} />
            <Route path="/site" element={<SitePage />} />
            {/* <Route path="/management" element={<RentalPage />} /> */}
            <Route path="/rental" element={<RentalPage />} />
            <Route path="/selection" element={<SelectionPage />} />
            <Route path="/places" element={<PlacesPage />} />
            <Route path="/tower" element={<TowerPage />} />
            <Route path="/management" element={<ManagerMyTower />} />
            <Route path="/technical" element={<DocumentsTechniques />} />

          </Routes>
        </AppProvider>
      </StateProvider>
    </BrowserRouter>
  );
}

export default App;
